(******************************************
*********
** Auteurs : Elio Maisonneuve
** But     : Un jeux de la vie
** Nom     : LifGame
** Date    : 30/01/2015
*********
******************************************)
PROGRAM LifGame;
uses Crt;

  type tableau = array of array of integer;
  
  VAR 
    lignes:integer=1000;
    colones:integer=1000;





  (*****************************************
  *********
  ** Auteurs         : Elio Maisonneuve
  ** Prototype       : initGrille( nbrViv:integer, dimension:integer ):array of array of boolean
  ** Pré-conditions  : nbrViv Est un entier positif, dimension un Entier Superieur a 3
  ** Post-conditions : Place les cellules vivante initiale dans la grille
  *********
  *****************************************)
  FUNCTION initGrille ( nbrViv:integer; dimension:integer ):tableau;
  VAR
    i,j: integer;
    nbrCell:integer;
    midLigne,midCol, midDimension: integer;
    ran,prob:real;
    grille:tableau;
    
  BEGIN
    setLength(grille, lignes, colones);
    nbrCell := dimension * dimension;
    writeln('demmarage');
    delay(100);
    
    midLigne := lignes div 2;
    midCol := colones div 2;
    midDimension := dimension div 2;
    FOR i:= (midLigne - midDimension) to  (midLigne + midDimension - 1) DO
    BEGIN
      FOR j:= (midCol - midDimension) to (midCol + midDimension - 1) DO
      BEGIN
        ran := random;
        prob := nbrViv / nbrCell;
        IF ( ran < prob ) THEN
        BEGIN
          grille[i][j] := 1;
          nbrViv := nbrViv - 1;
        END;
        nbrCell := nbrCell - 1;
      END;
    END;
    initGrille := grille;
  END;
 








  (*****************************************
  *********
  ** Auteurs         : Elio Maisonneuve
  ** Prototype       : chargeFile( nom:string ):tableau
  ** Pré-conditions  : Nom est une chaine comportant le nom du fichier a ouvrir
  ** Post-conditions : Charge un fichier dans la grille
  *********
  *****************************************)
  FUNCTION chargeFile ( nom:string ):tableau;
  VAR
    i,j:integer;
    fichier:TEXT;
    ligne:string;
    cell:string;
    grille:tableau;
  BEGIN
    setLength(grille, lignes, colones);
    assign(fichier, nom);
    reset(fichier);
    cell := '0';
    i := 1;
    WHILE ( cell <> '&' ) DO
    BEGIN
      read(fichier, ligne);
      j := 1;
      i := j + 1;
      cell := copy(ligne, j, 1);
      WHILE ( (cell <> '|' )) AND (cell <> '&') DO
      BEGIN
        j := j + 1;
        IF ( cell = 'g' ) THEN
        BEGIN 
          grille[i][j] := 1;
        END;
        cell := copy(ligne, j, 1);
        writeln(i);
      END;
    END;
    close(fichier);

    chargeFile := grille;
  END;
  
 








  (*****************************************
  *********
  ** Auteurs         : Elio Maisonneuve
  ** Prototype       : afficheGrille( grille:array of array of boolean, dimension:integer ):boolean
  ** Pré-conditions  : grille est un tableau de boolean longeur et largeur lignes, colones 
  ** Post-conditions : affiche l'etat de la grille
  *********
  *****************************************)
  PROCEDURE afficheGrille (grille:tableau; xPos,yPos:integer);
  VAR
    
    i,j:integer;
    lig:ansistring;
    
  BEGIN
    clrScr;
    
    //Sépare les ligne
    FOR j:=0 to 70 - 1 DO 
    BEGIN
      write('+-');
    END;
    writeln('+');


    IF ( (yPos + 139) >= lignes ) THEN
    BEGIN
      yPos := lignes - 140;
    END;
    IF ( (xPos + 339) >= colones ) THEN
    BEGIN
      xPos := colones - 340;
    END;
    
    FOR i:=yPos to yPos + 20 DO
    BEGIN
    lig := '';
      FOR j:=xPos to xPos + 140 DO //affiche les cases
      BEGIN
        //write ('|');
        IF ( grille[i][j] = 1 ) THEN
        BEGIN
          lig := lig + '#';
        END
        ELSE
        BEGIN
          lig := lig + ' ';
        END;
      END;
      textColor(green);
      writeln(lig);
      textColor(7);

    END;

    // Ajoute une dernière ligne pour fermer le tableau
    FOR j:=0 to 70 - 1 DO 
    BEGIN
      write('+-');
    END;
    writeln('+');

    write('X : ');
    write(xPos);
    write('  Y : ');
    writeln(yPos);
  END;
  









  (*****************************************
  *********
  ** Auteurs         : Elio Maisonneuve
  ** Prototype       : compteVoisi( i,j:integer ):integer
  ** Pré-conditions  : I et J sont des entier superieur a zero et inferieur respectivement a lignes et colones
  ** Post-conditions : Renvoi le nombre de cellule vivante voisine de la cellule aux coordonne passe en parametre
  *********
  *****************************************)
  FUNCTION compteVoisi ( i,j:integer; grille:tableau ):integer;
  VAR
    nbrVoisin:integer;
    k,l:integer;
  BEGIN
    nbrVoisin := 0;
    
    FOR k:=(i-1) to (i+1) DO
    BEGIN
      FOR l:=(j-1) to (j+1) DO
      BEGIN
        IF ( (k>=0) AND (k <= (lignes-1)) AND (l>=0) AND (l <= (colones-1)) ) THEN
        BEGIN
          nbrVoisin := nbrVoisin + grille[k][l];    
          
        END;
      END;
    END;

    compteVoisi := nbrVoisin - grille[i][j];
  END;








  (*****************************************
  *********
  ** Auteurs         : Elio Maisonneuve
  ** Prototype       : etatCell( i,j:integer; grille:tableau ):boolean
  ** Pré-conditions  : I et J sont les coordonee de la cellule a verifier, grille le tableau dans laquelle est la cellule
  ** Post-conditions : Renvoi vrai si la cellule est vivante, faux sinon
  *********
  *****************************************)
  FUNCTION etatCell ( i,j:integer; grille:tableau ):boolean;
  VAR
    nbrVoisin:integer;
    etat:boolean;
  BEGIN
    nbrVoisin := compteVoisi(i,j,grille);
    etat := false;
    IF ( nbrVoisin = 3 ) THEN
    BEGIN
      etat := true;
    END
    ELSE
    BEGIN
      IF ( (nbrVoisin = 2) AND (grille[i][j] = 1) ) THEN
      BEGIN
        etat := true;
      END;
    END;
    
    etatCell := etat;
  END;
  








  (*****************************************
  *********
  ** Auteurs         : Elio Maisonneuve
  ** Prototype       : cycle( grille:tableau, iteration:integer )
  ** Pré-conditions  : grille est une grille de taille lignes et colones initialisé, iteration et un entier superieur ou egal a un
  ** Post-conditions : Calule chaque cycle de vie
  *********
  *****************************************)
  FUNCTION cycle ( grilleCur:tableau):tableau;
  VAR
    i,j:integer;
    grilleNext:tableau;
  BEGIN
    setLength(grilleNext, lignes, colones);
    FOR i:=0 to lignes - 1 DO
    BEGIN
      FOR j:=0 to colones - 1 DO
      BEGIN
        IF ( etatCell(i,j,grilleCur) ) THEN
        BEGIN
          grilleNext[i][j] := 1;
        END;
      END;
    END;

    cycle := grilleNext;
  END;
  




  (*****************************************
  *********
  ** Auteurs         : Elio Maisonneuve
  ** Prototype       : choixMode( ):integer
  ** Pré-conditions  : 
  ** Post-conditions : 
  *********
  *****************************************)
  (*FUNCTION choixMode (  ):integer;
  VAR
    mode:integer;
    choisi:boolean = false;
  BEGIN
    mode := 1;
    while
    clrScr;
    WHILE ( NOT (choisi) ) DO
    BEGIN
      clrScr;
      gotoXY(40,20);
      IF ( mode = 1 ) THEN
      BEGIN
        textColor(black);
        textBackground(white);
        write('Placement aleatoire ');
        textColor(7)
        textBackground(black);
        write(' Charger fichier grille');
        gotoXY(40,23);
        
      
      END;
    END;
    gotoXY(40,20);
    
    
  END;*)
  






VAR
  dimension:integer;
  nbrViv:integer;
  xPos,yPos:integer;
  gen:integer;
  touche:char;
  enjeu:boolean;
  grilleCur,grilleNext:tableau;
BEGIN
  randomize;
  clrScr;
  gotoXY(60,20);
  writeln ('Entrez La taille de la grille :');
  gotoXY(60,21);
  readln (dimension);
  gotoXY(60,23);
  writeln ('Population Initiale :');
  gotoXY(60,24);
  readln (nbrViv);

  setLength (grilleCur, lignes, colones);
  setLength (grilleNext, lignes, colones);
  grilleCur := initGrille(nbrViv,dimension);
  //grilleCur := chargeFile('grille.txt');
  xPos := 430;
  yPos := 480;
  gen := 1;
  touche := '0';
  enjeu := true;
  WHILE ( enjeu ) DO
  BEGIN
    gen := gen + 1;
    grilleCur := cycle(grilleCur);


    IF ( keyPressed ) THEN
    BEGIN
      touche := readKey;
      CASE touche OF
        #0 :  BEGIN
                touche := readKey;
                CASE touche OF
                  #75 : xPos := xPos - 10;
                  #77 : xPos := xPos + 10;
                  #72 : yPos := yPos - 10;
                  #80 : yPos := yPos + 10;
                END;
              END;
        'q' : enjeu := false;
        'r' : BEGIN
                grilleCur := initGrille(nbrViv,dimension);
                xPos := 430;
                yPos := 480;
                gen := 0;
              END;

      END;
      
      afficheGrille(grilleCur, xPos, yPos);
      writeln(gen);

      WHILE ( keyPressed ) DO
      BEGIN
        touche := readKey;
      END;
    END;
  END;
END.

