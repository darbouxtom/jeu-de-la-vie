program jeuDeLaVie;

uses crt,mouse,cyclis,unix,sysutils;



type cellule = boolean;

type ligne = array of cellule;
type grille = array of ligne;//une grille de booleen false:mort true:vivant
type g2 = array of array of integer;//une grille a trois états 0:mort 1:vivant 2:curseur
type phrase = array of string;

var
  lignes01:integer;
  colones01:integer; 




  (*****************************************
  *********
  ** Auteurs         : Elio Maisonneuve
  ** Prototype       : initGrille( nbrViv:integer, dimension:integer ):array of array of boolean
  ** Pré-conditions  : nbrViv Est un entier positif, dimension un Entier Superieur a 3
  ** Post-conditions : Place les cellules vivante initiale dans la grille
  *********
  *****************************************)
  FUNCTION initGrille ( nbrViv:integer; dimension:integer ):grille;
  VAR
    i,j: integer;
    nbrCell:integer;
    midLigne,midCol, midDimension: integer;
    ran,prob:real;
    tableau:grille;
    
  BEGIN
    setLength(tableau, lignes01, colones01); //Creation du petit tableau
    nbrCell := dimension * dimension; //Calcul du nombre total de cellule
    writeln('demmarage'); 
    delay(100);
    
    midLigne := lignes01 div 2; //On calcul la position du milieu
    midCol := colones01 div 2;
    midDimension := dimension div 2;
    FOR i:= (midLigne - midDimension) to  (midLigne + midDimension - 1) DO //On va parcourir le tableau en se placant au milieu
    BEGIN
      FOR j:= (midCol - midDimension) to (midCol + midDimension - 1) DO
      BEGIN
        ran := random; //On prend un combre aleatoire entre 0 et 1
        prob := nbrViv / nbrCell; //on Calule la probabilité qu il y ai une cellule vivante dans la case courante
        IF ( ran < prob ) THEN //Si le nombre aleatoire est inferieur a la probabilité
        BEGIN
          tableau[i][j] := true; //La cellule est vivante
          nbrViv := nbrViv - 1; //Il reste une cellule vivante de moins à placer
        END;
        nbrCell := nbrCell - 1; //Il reste une cellule de moins a parcourir
      END;
    END;
    initGrille := tableau; //On renvoi le tableu rempli
  END;


(* PRECONDTIONTION: --t:ligne initialisé
 
*)

(* POSTCONDITIONS: afficher une ligne(booleen) vrai:affiche un '°' faux:affiche un espace*)


procedure afficheTab (t : ligne);
var
longueur: integer;
i: integer;
begin
  //write('||');
  longueur := Length(t) -1 ; //Récupère la longeur d une ligne
  for i:= 0 to longueur do  //On parcour la ligne
  begin
    if t[i] //Si la cellule est vivante
    then
    begin
      (*On place un o symbolisant une cellule*)
      textColor(random(14) + 1);
      write('o');
      textColor(7);
    end
    else  //Si la cellule est morte, on place un espace
    begin      
      write(' ');      
    end;
    //write('||');
  end;

end;

(* PRECONDTIONTION: --tab:grille initialisé, represente la position des cellules
 
*)

(* POSTCONDITIONS: afficher grille de type Grille (booleen) , vrai:affiche un '°' faux:affiche un espace*)

procedure afficheBigTab (t : grille);
var
long,long2 : integer;
i,j: integer;
begin
  long2 := Length(t[1]);  //Récupère la longeur d une ligne
  for j:=0 to long2 do  //On affiche la barre du haut
  begin
    write('-');
  end;
  writeln(''); //Retour à la ligne
  long := Length(t)-1; //Récupère le nombre de ligne
  for i:= 0 to long do //Pour chaque ligne
  begin
    afficheTab(t[i]); //On affiche la ligne
    writeln(''); //Retour a la ligne    
  end;
  writeln();
  for j:=0 to long2 do  //On affiche la barre du bas
    write('-'); 
end;

(* PRECONDTIONTION: --tab:grille initialisé, represente la position des cellules

*)

(* POSTCONDITIONS: affiche une grille de type G2 (entier <= 2), 2:affiche un '+' 1:affiche un '°' 0:affiche un espace*)

procedure afficheBigTab02 (t : g2);
var
long,long2 : integer;
i,j,k: integer;
begin
   long2 := Length(t[1]);
   for j:=0 to long2 do
   begin
      write('-');
   end;

   long := Length(t)-1;
   for i:= 0 to long do //Pour chaque ligne
   begin      
      writeln('');      
      for k:= 0 to long2 do //Pour chaque case de chaque ligne
      begin
         if (t[i][k] = 1)
         then
         begin
            textColor(green);
            write('o');
            textColor(7);
         end
         else if (t[i][k] = 2) then //Si le curseur est à cet endroit
         begin
            write('+'); //On affiche un + simbolisant le curseur
         end
         else
         begin
            write(' ');
         end;
         //write('||');
      end;
   end;
   writeln();
   for j:=0 to long2 do
   begin
      write('-');
   end;
   writeln('');
end;



(* PRECONDTIONTION: --tab:grille initialisé, represente la position des cellules
                    --v,w:entier positifs, position de la fenètre à afficher
*)

(* POSTCONDITIONS: afficher un bout de grille de type Grille (booleen), vrai:affiche un '°' faux:affiche un espace*)

procedure afficheBigBigTab(tab:grille;v,w,petitx,petity:integer);
var petitTab : grille;
    i,j : integer;
    

begin
  setLength(petitTab,petitx,petity); //Initialisation du tableau
  for i:=0 to petitx-1 do //On parcours tout le tableau
    for j:=0 to (petity-1) do 
      begin
      petitTab[i][j] := tab[v+i][w+j];  //On remplis le tableau avec les cellules du grand
      end;
  afficheBigTab(petitTab); //On affiche le tableau reduit

end;


(* PRECONDTIONTION: --tab:grille initialisé, represente la position des cellules
                    --v,w:entier positifs, position de la fenètre à afficher
*)

(* POSTCONDITIONS: affiche un bout de grille de type G2 (entier <= 2), 2:affiche un '+' 1:affiche un '°' 0:affiche un espace*)


procedure afficheBigBigTab02(tab:g2;v,w,petitx,petity:integer);
var petitTab : g2;
    i,j : integer;

begin
  setLength(petitTab,petitx,petity);//on initialise un petit tableau qui represente la fenètre

  //on transfere les cases à afficher du grand tableau dans le petit tableau
  for i:=0 to petitx-1 do
    for j:=0 to petity-1 do
      petitTab[i][j] := tab[v+i][w+j];


  afficheBigTab02(petitTab);//on affiche le petit tableau

end;


(******************************************************************************************)
(*PRECONDITION:--v,w,x,y:entier
               --long,long2 :entier positifs, rempresente la taille de la grille
               --petitx,petity:entier positifs, represente la taille de la fenetre

  POSTCONDITION:la fonction initialise v,w,x,y en fonction des tailles
  *)

procedure initialisationCoord_Inittab03(var v,w,x,y:integer;long,long2,petitx,petity:integer);
begin
  //on initialise les coordonnés de la fenètre afin qu'il soit centrés
  v := ((long div 2 - (petitx div 2))+long)mod long;
  w := ((long2 div 2 - (petity div 2))+long2)mod long2;
  x := 1;
  y := 1;
end;

(*PRECONDITION:--t:Grille initialisé
               --long,long2 :entier positifs, rempresente la taille de la grille
               --t2 grille de type G2(entier entre 0 et 2)

  POSTCONDITION:la fonction transforme une Grille en t2*)

procedure GrilleToG2(t:grille;var t2:G2;long,long2:integer);
var
i,j:integer;
begin
  setLength(t2,long,long2);//on cree une nouvelle grille ou l'on placera le curseur qui servira pour l'affichage (la grille booleene sert à memoriser l'emplacemnt des cellule)
  for i:=0 to long -1 do
  begin
    for j:=0 to long2 -1 do
    begin
      if(t[i][j]) then t2[i][j] := 1;//si il ya une cellule vivante dans la première (vrai) on la met dans la deuzieme (=1)
      if(not(t[i][j])) then t2[i][j] := 0;//si il ya une cellule morte dans la première (faux) on la met dans la deuzieme (=0)
    end;
  end;
end;

(* PRECONDITION,  
                -- x,y:entier ,represente la taille de la grille
                -- petitx,petity :entier ,represente la taille de l'affichage
                -- temps:entier positifs, temps de pause a chaque cycle
                -- nom:chaine de caractère non vide,nom de la sauvegarde
                -- nomFich:chaine de caractère non vide,nom du fichier de sauvegarde
                
*) 
(* POSTCONDITION: fonction qui enregistre dans un fichiers les differente données*)





PROCEDURE ecrireSave(g:grille;x,y,petitx,petity,temps:integer;nom,nomFich:string);
var
f:TextFile;
i,j:integer;
ligne : string;
begin
  assign(f,nomFich);//on ouvre le fichier         
  rewrite(f);//on l'initialise
  writeln(f,nom);//on ecrit le nom de la sauvegarde en haut
  writeln(f,IntToStr(x));//puis la taille de la grille
  writeln(f,IntToStr(y));
  writeln(f,IntToStr(petitx));//puis la taille de la fenetre d'affichage
  writeln(f,IntToStr(petity));
  writeln(f,IntToStr(temps));//et enfin le temps de pause
  
  //on passe aux chose serieuse, on va écrire la grille dans me fichier
  for i:=0 to x-1 do
  begin
    ligne := '';//on commence la ligne par rien
    for j:=0 to y-1 do
      if g[i][j] then ligne := ligne + '1' else ligne := ligne + '0';//ecris la ligne caractere par caratere
    writeln(f,ligne);//on ecris la ligne qu'on vient de creer
  end;
  close(f);//on ferme le fichier
end;



(* PRECONDITION,  
                -- x,y:entier ,represente la taille de la grille
                -- petitx,petity :entier ,represente la taille de l'affichage
                -- temps:entier positifs, temps de pause a chaque cycle
                
*) 
(* POSTCONDITION: fonction qui propose à l'utilisateur d'enregistrer dans un fichiers sa propre figure*)




PROCEDURE save(g:grille;x,y,petitx,petity,temps:integer);
var
f:TextFile;
nom:string;
numero:string;
nomFich,commande:string;
begin
  clrScr();
  writeln('enregistrement du fichier');
  writeln('=========================');
  write('nom de la sauvegarde?');
  readln(nom);

  numero := lireFichier(1,'save.config');

  assign(f,'save.config');//on ouvre le fichier         
  rewrite(f);//on l'initialise
  writeln(f,IntToStr(StrToInt(numero)+1));//on augmente le numero dans le fichier
  close(f);//on ferme le fichier
  nomFich := 'schema/'+numero+'.vie';//on créé le nom pour notre fichier
  commande := 'touch '+nomFich;//on créé la commande pour cree le fichier
  fpsystem(commande);//on éxecute la commande
  ecrireSave(g,x,y,petitx,petity,temps,nom,nomFich);
  writeln('fichier enregistré');
  delay(1000);

end;











(* PRECONDITION -- t:grille, represente le placement des cellules
                             
*)
(* POSTCONDITION:initialisation par l'utilisateur de la grille t *)

procedure inittab03(var t:grille;petitx,petity,temps:integer);
var
w,v,x,y,long,long2:integer;
touche:char;
t2:g2;//une grille diferente de la grille en parametre qui servira pour l'affichage (du curseur)

begin
  
  long := Length(t);//on recupere la largeur de la grille
  long2 := Length(t[0]);//on recupère sa longueur
  
  initialisationCoord_Inittab03(v,w,x,y,long,long2,petitx,petity);//on initialise les coordonnées  
  GrilleToG2(t,t2,long,long2);//on copie la grille booleenne vers la grille d'affichage
  t2[v+x][w+y] := 2;//on place le curseur (=2)
  touche := 'u';
  clrscr(); //on efface l'ecran
  afficheBigBigTab02(t2,v,w,petitx,petity);//on affiche un fenetrage de la grille d'affichage 
  while (touche <> 'q') do//tant que l'utilisateur n'appuie pas sur la touche pour quitter 'q'
  begin
    
    if (KeyPressed) then //si une touche a été préssé
    begin
      writeln('');//on saute une ligne
      //on remplace la valeur du curseur par la valeur de la cellule (morte ou vivante) a partir de la grille booleenne
      if(t[v+x][w+y]) then t2[v+x][w+y] := 1;
      if(not(t[v+x][w+y])) then t2[v+x][w+y] := 0;

      touche:=Readkey;//on recupere la touche
      case touche of
          #0://si la touche est bizzare
          begin
            touche := ReadKey;//on recupere la suite de la touche
            case touche of
              #75 : y:= y-1;//si on appuie sur la touche gauche on diminue les coordonnés horizontaux du curseur
              #77 : y:= y+1;//si on appuie sur la touche droite on augmente les coordonnés horizontaux du curseur
              #72 : x:= x-1;//si on appuie sur la touche haut on diminue les coordonnés verticaux du curseur
              #80 : x:= x+1;//si on appuie sur la touche bas on augmente les coordonnés verticaux du curseur
              #83 : t[v+x][w+y] := false;//si on appuie sur suppr, on retire la cellule de la grille booleenne
            end;
          end;
          #13:t[v+x][w+y] := true;//si on appuie sur entrée, on ajoute une cellule à la grille booleenne
          ' ':t[v+x][w+y] := true;//de meme si on appuie sur la barre d'espace          
          'r':save(t,long,long2,petitx,petity,temps);
      end;      
      
      //si le curseur sort de la grille ...
      if(x>petitx) then //...par le bas
      begin
          x:=petitx;
          v:=v+1;//on deplace la position de la fenetre vers le bas
      end;
      if(y>petity) then //...par la droite
      begin
          y:=petity;
          w:=w+1;//on deplace la position de la fenetre vers la droite
      end;
      if(x<0) then //...par le haut
      begin
          x:=0;
          v:=v-1;//on deplace la position de la fenetre vers le haut
      end;
      if(y<0) then //...par la gauche
      begin
          y:=0;
          w:=w-1;//on deplace la position de la fenetre vers la gauche
      end;
      //on empeche la fenetre de sortir du tableau
      if (v>long2 - petitx) then v:=long2 -petitx;
      if (w>long - petity) then w:=long - petity;
      while keypressed do touche:=Readkey;//on vide les touches dans le buffer sinon ca pose des problemes
      t2[v+x][w+y] := 2;//on place le cursur du la grille d'affichage      
      clrscr(); //on efface l'ecran
      afficheBigBigTab02(t2,v,w,petitx,petity);//on affiche un fenetrage de la grille d'affichage
      writeln('q:enregistrer et commencer le programme');
      writeln('r:enregistrer la grille'); 
    end;
  end;
end;

(*==========le cycle===========*)

(* PRECONDITION -- t:grille initialisé , represente le placement des cellules
                -- x,y:entier positifs , position dans la grille de la cellule
*)
(* POSTCONDITION:retourne le nombre de voisin de la cellule positionnée dans la grille par (x,y)*)
function voisin(t:grille;x,y:integer):integer;
var
i,j,r,a,b:integer;
begin
  r:=0;
  //on parcours toutes les cellules dans le carre de rayon +-1 autour du centre
  for i:=-1 to 1 do
  begin
    for j:=-1 to 1 do
    begin
    a := x+i;
    b:=y+j;
    //pour compter les cellule de l autre coté de la grille
    IF ( (a>=0) AND (a <= (lignes01-1)) AND (b>=0) AND (b <= (colones01-1)) ) THEN //Si la case est bien dans le tableau
    BEGIN
      if(t[a][b]) then//si il ya une cellule
      begin
        r := r+1;//on ajoute un voisin
      end;
    END;
    end;
  end;
  if (t[x][y]) then r:=r-1;//si on compte le nombre de voisin d'une cellule vivante on retire un voisin car on n'est pas son propre voisin
  voisin := r;//on renvoie le nombre de voisin
end;



(* PRECONDITION -- t:grille initialisé , represente le placement des cellules
                -- x,y:entier positifs , taille de la grille                
*)
(* POSTCONDITION:avance le jeu d'un cycle*)

function cycle(t:grille;x,y:integer):grille;
var
i,j:integer;
t2 : grille;
begin
  setLength(t2,x,y);//on créé la grille
  
  
  //on parcours la grille
  for i:= 0 to x-1 do
  begin
    for j:= 0 to y-1 do
    begin
      case (voisin(t,i,j)) of//on compte le nombre de voisin 
        2:    t2[i][j] := t[i][j];
        3:    t2[i][j] := true;
        else  t2[i][j] := false;  //si il ya pas 2 ou 3 voisin on supprime la cellule
      end;
    end;    
  end;
  cycle := t2;//on renvoie la grille

end;




(* PRECONDITION -- a:entier positifs
                -- b:entier positifs
                -- c:entier vide
*)
(* POSTCONDITION:demande la variable 'c' à l'utilisateur a l'emplacement chois (a,b)*)



procedure petitpoints(a,b:integer;var c:integer);
begin
  gotoxy(a,b);//on va a la fin de la ligne
  write('......');
  gotoxy(a,b);//on va avant les points
  readln(c);

end;

(* PRECONDITION -- maxt:entier , represente le nombre de tours à jouer 
                -- x,y:entier ,represente la taille de la grille
                -- pop:entier ,population de depart
                -- pause:entier , temps de pause a chaque cycle
                -- utilisateur:booleen ,vraie=initialisation manuelle faux=initialisation automatique                
*)
(* POSTCONDITION:affiche le menu*)


procedure affmenu(x,y,petitx,petity,gene01,pause,pop:integer;utilisateur:boolean);
begin
  clrscr();
  writeln('Jeu de la vie');
  writeln('=============');
  write('  placement manuel:');//on place le choix sur le premier element
  
  if(utilisateur) then writeln('oui') else writeln('non');//si le placement est manuelle on affiche oui sinon non
  write('  hauteur:');
  writeln(x);
  write('  largeur:');
  writeln(y);
  write('  petite hauteur:');
  writeln(petitx);
  write('  petite largeur:');
  writeln(petity);
  write('  taille generation:');
  writeln(gene01);
  write('  temps:');
  writeln(pause);
  if(not(utilisateur))then//si le placement est automatique
  begin
  write('  population:');//on affiche la ligne pour demander la poulation totale
  writeln(pop);
  end;
  writeln('  valider');
  writeln('  quitter');

end;


(* PRECONDITION -- maxt:entier , represente le nombre de tours à jouer 
                -- x,y:entier ,represente la taille de la grille
                -- pop:entier ,population de depart
                -- pause:entier , temps de pause a chaque cycle
                -- utilisateur:booleen ,vraie=initialisation manuelle faux=initialisation automatique 
                -- petitx,petity:entier, taille de la fenetre affiché
                --gene01:entier , taille (carré) de la generation de depart
*)
(* POSTCONDITION:initialise toute les variable sous forme de menu interactif*)


procedure interface02(var petitx,petity,gene01:integer;var x,y,pop,pause:integer;var utilisateur:boolean);
var
touche:char;
z:integer;
begin
  (*on initialise les valeurs*)
  pause:=50;
  x:=20;
  y:=20;
  z:=1;
  petitx:=15;
  petity := 15;
  pop:=40;
  gene01 := 10;
  utilisateur := false;

  touche := 'v';//on initalise la touche appuyer par le clavier par un caractere benin
  clrscr;


  (*on affiche le menu une première fois*)
  affmenu(x,y,petitx,petity,gene01,pause,pop,utilisateur);
  gotoxy(1,3);//on se deplace sur la bonne ligne
  write('>');//on afiiche le curseur
  while (((not((z=9) or (z=8)))and(utilisateur))or((not((z=10) or (z=9)))and(not(utilisateur))) or (touche <> #13)) do//on continue si on clique pas sur la touche entréé a la ligne 'quitter', celle-ci n etant pas placer au meme endroit si le placement est manuelle ou automatique
  begin
      if (KeyPressed) then //si une touche est détecté
      begin
        touche:=Readkey;//on recupère la touche
        case touche of
            #13 ://on appuie sur entrée
            begin               
               case z of
                  2://hauteur grille
                  begin
                  petitpoints(11,z+2,x);
                  end;
                  3://largeur grille
                  begin
                  petitpoints(11,z+2,y);                  
                  end;
                  4://hauteur fenetre
                  begin
                  petitpoints(18,z+2,petitx);
                  end;
                  5://largeur fenetre
                  begin
                  petitpoints(18,z+2,petity);
                  end;
                  6://coté du carré ou seront generés les cellules de depart
                  begin
                  petitpoints(21,z+2,gene01);
                  end;
                  7://temps de pause
                  begin
                  petitpoints(9,z+2,pause);
                  end;
                  8://la population de depart
                  begin
                    if(not(utilisateur)) then
                    begin
                    petitpoints(14,z+2,pop);
                    end;
                  end;

               end;
               if(utilisateur and (z=9)) or (not(utilisateur)and (z=10)) then halt();//si on a choisi 'quitter' on quitte le jeu
            end;
      
            #0://si c est une touche 'bizzare'
            begin
              
              touche := ReadKey;//on lit la touche
              case touche of
                #75 ://droite
                begin
                  case z of
                    1:
                    begin
                    //on change la valeur
                    utilisateur := not(utilisateur);
                    end;
                    2:
                    begin
                      x:=x-1;//on diminue les valeurs de la ligne
                    end;
                    3:y:=y-1;//on diminue les valeurs de la ligne
                    7:pop:=pop-1;//on diminue les valeurs de la ligne
                    4:petitx:=petitx-1;//on diminue les valeurs de la ligne
                    5:petity:=petity-1;//on diminue les valeurs de la ligne
                    6:gene01:=gene01-1;
                  end;
                end;
                #77 ://gauche
                begin
                  case z of
                    1:
                    begin
                    //on change la valeur
                    utilisateur := not(utilisateur);
                    end;
                    2:
                    begin
                      x:=x+1;//on augmente les valeurs la ligne
                    end;
                    3:y:=y+1;//on augmente les valeurs la ligne
                    7:pop:=pop+1;//on augmente les valeurs la ligne
                    4:petitx:=petitx+1;//on augmente les valeurs la ligne
                    5:petity:=petity+1;//on augmente les valeurs de la ligne
                    6:gene01:=gene01+1;
                  end;
                end;
                #72 : z:= z-1;//haut : on diminue la coordonné vertical
                #80 : z:= z+1;//bas : on augmente la coordonné vertical
              end;
            end;
        end;
        if(utilisateur and (z>9)) then z:=9;//si c'est en mode manuelle et que le curseur depasse la 9eme ligne on le replace a la 9eme
        if((not(utilisateur)) and (z>10)) then z:=10;//si c'est en mode manuelle et que le curseur depasse la 10eme ligne on le replace a la 10eme
        if(z<1) then z:=1;
        if(petitx > x) then petitx:=x;
        if(petity > y) then petity:=y;
        if(gene01 > petitx) then gene01 := petitx;
        if(gene01 > petity) then gene01 := petity;

      //on actualise le menu
      clrscr;
      affmenu(x,y,petitx,petity,gene01,pause,pop,utilisateur);
      gotoxy(1,z+2);//on se deplace sur la bonne ligne
      write('>');//on afiiche le curseur

      end; 
      
  end;
  

end;

(*PRECONDITION:--touche:caractere representant une touche appuyé
               --x,y,v,w :entier positifs, represente des coordonnés
               --petitx,petity:entier positifs, taille de la fenètre

  POSTCONDITION:la fonction modifie v,w en fonction de la touche recu
  *)


procedure actionTouche(touche:char;var v,w:integer;x,y,petitx,petity:integer);
begin
  if(touche = #0) then//si c est une touche 'etrange'
  begin
    touche := ReadKey; //on recupere la deuzieme info de touche
    case touche of
      #72 : v:= v-1;//si on appuie sur haut on diminue les coordonnées verticaux de la fenetre
      #80 : v:= v+1;//si on appuie sur bas on augmente les coordonnées verticaux de la fenetre
      #75 : w:= w-1;//si on appuie sur gauche on diminue les coordonnées horzontaux de la fenetre
      #77 : w:= w+1;//si on appuie sur droite on augmente les coordonnées horizontaux de la fenetre
    end;
    (*ici on empeche l'utilisateur de sortie de la grille*) 
    if (w<0) then w:=0;//si les coordonnés horizontaux vont trop a droite 
    if (v<0) then v:=0;//si les coordonnés verticaux vont trop en haut
    if (v>x-petitx ) then v:=x-petitx;//si les coordonnés verticaux vont trop en bas
    if (w>y-petity ) then w:=y-petity; //si les coordonnés horizontaux vont trop a gauche
    while keypressed do touche:=Readkey;//on vide le buffer sinon ca pose des problemes plus tard
  end;
end;






(* PRECONDITION -- maxt:entier > 0 , represente le nombre de tours a jouer 
                -- x,y:entier positifs ,represente la taille de la grille
                -- v,w:entier positifs ,represente la position de la sous fenetre si grille trés grande
                -- pop:entier positifs ,population de depart
                -- temps:entier positifs, temps de pause a chaque cycle
                -- utilisateur:booleen initialisé,vraie=initialisation manuelle faux=initialisation automatique
                -- tab:grille initialisée de taille (x,y) ,represente le placement des cellules
*) 
(* POSTCONDITION: procedure qui joue : ne renvoie rien*)

procedure deroulement(petitx,petity,x,y,v,w,temps : integer ; utilisateur:boolean;tab:grille);
var
touche : char;
i:integer;
begin
  touche := 'u';//on initalise la touche appuyer par le clavier par un caractere benin
  i:=1;
  while ((touche <> 'a')and(touche <> #27)) do //si on depasse pas le nombre de tours et l utilisateur appuie pas sur 'a'
  begin
    tab := cycle(tab,x,y);//on fait un cycle
    clrscr();
    
    afficheBigBigTab(tab,v,w,petitx,petity);//on affiche un bout du tableau 
     
    delay(temps);//on attend le temps demandé par l utilsateur pour pas que ca aille trop vite        
    touche := 'u';////on initalise la touche appuyer par le clavier par un caractere benin
    i:=i+1;//on incremente le nombre de tour
    if (KeyPressed) then touche := ReadKey;//si une touche est détecté on la recupère
    actionTouche(touche,v,w,x,y,petitx,petity);

  end;
end;

//fonction pour gerer les fichiers externes...


(* PRECONDITION, elle ne prend aucun parametre, une idéé d'amelioration pourrait être le fait de demander le chemin
*) 
(* POSTCONDITION: fonction qui liste les fichiers de sauvegarde dans le dossier du programme et renvoie les nom sous forme de liste de chaine*)
function listefile():phrase;
var
texte:string;
fichier:TextFile;
name01:phrase;
i,taille :integer;
begin

  fpsystem('ls schema/ > /tmp/fichiers.temp01');//on liste les fichiers
  
  taille := nombreligne('/tmp/fichiers.temp01');
  //writeln('hihi');
  Assign(fichier, '/tmp/fichiers.temp01');//on ouvre le fichier
  reset(fichier); //on initialise le fichier
  //writeln('huhu');
  setLength(name01,taille);//on crée un tableau pour srocker les noms des fichiers
  texte :=  'hjiozad';
  i := -1;

  while not((texte = '')) do//tant qu'on arrive pas a la fin du fichier
  begin
    readln(fichier,texte);//on lit la prochaine ligne
    if(extension(texte) = 'vie') then//si l'extnsion est valide
    begin
      i := i+1;
      texte := 'schema/' + texte;
      name01[i] := texte;//on stocke le nom
    end;
  end;
  Close(fichier);//on ferme le fichier
  listefile := name01;//on envoie la liste

end;

(* PRECONDITION, --list est un tableau de chaine, ce doit être des noms de fichiers accessible
*) 
(* POSTCONDITION: fonction qui transforme la liste de fichier en menu*)

function menuFile(list : phrase):string;
var
l,i:integer;
texte : string;
num : integer;
begin
  l := Length(list);//on prend le nombre de fichiers
  writeln ('menu');
  writeln('====');
  for i:=0 to l-1 do//on parcours toute la liste
  begin
  write(i);
  write(':');
  texte := lireFichier(1,list[i]); //on prend la premeière ligne de chaque fichier
  writeln(texte);
  end;
  write('?');
  readln(num);//on demande a l utilisateur quelle fichier il veur utiliser
  while (num < 0) or (num>l-1) do//tant que le nombre est incorrect
  begin
    write('?');
    readln(num);//on redemande
  end;
  menuFile := list[num];//on envoie le nom du fichier
end;

function fichierToGrille(name :string):grille;
var
  tableau:grille;
  nbrligne,nbrCol:integer;
  i,j,k,l:integer;
  colDepart:integer;
  ligne, cell:string;
begin
  nbrLigne := nombreligne02(name);
  nbrCol := length(lireFichier(7, name));

  lignes01 := StrToInt(lireFichier(2,name));
  colones01 := StrToInt(lireFichier(3,name));
  setLength(tableau, lignes01, colones01);
  write(lignes01);
  write(':');
  writeln(colones01);
  k := ( lignes01 div 2 ) - ( ( nbrLigne - 6 ) div 2 );
  colDepart := ( colones01 div 2 ) - ( nbrCol div 2 );
  write(k);
  write(':');
  writeln(colDepart);
  delay(500);
  FOR i:=7 to nbrLigne DO
  BEGIN
    ligne := lireFichier(i, name);
    l := colDepart;
    FOR j:=0 to nbrCol DO
    BEGIN
      
      cell := RightStr(LeftStr(ligne,j),1);
      
      tableau[k][l] := (cell = '1');
      
      l := l + 1;
    END;
    k := k + 1;
  END;

  fichierToGrille := tableau;
end;


(* PRECONDITION,  
                -- x,y:entier ,represente la taille de la grille
                --petitx,petity :entier ,represente la taille de l'affichage
                -- temps:entier positifs, temps de pause a chaque cycle
                
*) 
(* POSTCONDITION: fonction qui charge les fichiers dee sauvegarde, initialise toutes les données nescessaire et renvoie la grille*)
function chargeFile(var x,y,petitx,petity,temps: integer):grille;
var
nom:string;
taille : integer;
begin
  clrScr();
  nom := menuFile(listefile());
  taille := nombreligne02(nom);
  writeln(taille);
  delay(1000);
  if (taille < 7) then
  begin
  writeln('fichier corrompu');
  halt();
  end;
  x := StrToInt(lireFichier(2,nom));
  y := StrToInt(lireFichier(3,nom));
  petitx := StrToInt(lireFichier(4,nom));
  petity := StrToInt(lireFichier(5,nom));
  temps := StrToInt(lireFichier(6,nom));
  writeln('chargeFile ok');
  chargeFile := fichierToGrille(nom);
end;



(* PRECONDITION -- maxt:entier positif, represente le nombre de tours à jouer 
                -- x,y:entier positifs,represente la taille de la grille
                -- pop:entier positif,population de depart
                -- temps:entier positif, temps de pause a chaque cycle
                -- utilisateur:booleen ,vraie=initialisation manuelle faux=initialisation automatique 
                -- petitx,petity:entier positifs, taille de la fenetre affiché
                --gene01:entier positif, taille (carré) de la generation de depart

  POSTCONDITION:Fonction demmarre le jeu selon les conditions passés en paramètre, ne renvoie rien;
*)


procedure debut01(petitx,petity,gene01,x,y,pop,temps:integer;utilisateur:boolean);
var
tableau : grille;
v,w:integer;
begin

    setLength(tableau,x,y);//on initialise a la bonne dimension    
    clrscr();    
    
    //on met les dimension en variable global
    lignes01 := y;
    colones01 := x;
    
    if(utilisateur) then //si l initialisation est manuelle
      inittab03(tableau,petitx,petity,temps)
    else //si l initialisation est automatique
      tableau := initGrille(pop,gene01);

    (*on initialise la position de la fenetre au milieu*)
    v := ((x div 2 - (petitx div 2))+x)mod x;
    w := ((y div 2 - (petity div 2))+y)mod y;
    
    deroulement(petitx,petity,x,y,v,w,temps, utilisateur,tableau);//on lance le jeu    

end;


(*PRECONDITION: ne prend pas de paramètres
  POSTCONDITION:Fonction qui va demmarrer le jeu selon les souhaits de l'utilisateur, ne renvoie rien;
*)
procedure menu02();
var
  tableau:grille;
  x,y,v,w:integer;
  pop:integer;
  utilisateur:boolean;
  temps:integer;
  petitx,petity:integer;
  gene01:integer;
  touche : char;
  z:integer;
begin
  //on initilaise les variables
  temps:=50;
  x:=20;
  y:=20;
  z:=1;
  petitx:=15;
  petity := 15;
  pop:=40;
  gene01 := 10;
  utilisateur := false;
  z:=2;

  while (z = 2) do//tant qu'on est pas sur jouer ou charger ou quitter
  begin
    touche := 'm';//on initilialise la touche sur une valeur benine
    z:=1;
    //on affiche le menu
    clrscr();
    writeln('Bonjour');
    writeln('-------');
    writeln(' :Demmarer la partie');
    writeln(' :option');
    writeln(' :charger une sauvegarde');
    writeln(' :quitter');
    //tant qu'on appuie pas sur entrée
    while(touche <> #13) do 
    begin
      gotoxy(1,z+2);//on place le curseur
      writeln('>');
      gotoxy(1,7);
      repeat//on attend une touche
      until KeyPressed;
      gotoxy(1,z+2);//on efface le curseur
      writeln(' ');
      touche := ReadKey;//on lit la touche
      if(touche =  #0) then//si c'est une touche speciale
      begin
        touche := Readkey;//on relit la touche      
        case touche of
          #72 : z:= z-1;//haut : on diminue la coordonné vertical
          #80 : z:= z+1;//bas : on augmente la coordonné vertical
        end;
      end;
      if(z<1) then z:=1;//si le curseur est trop haut
      if(z>4) then z:=3;//si il est trop bas

    end;
    if(z=1) then debut01(petitx,petity,gene01,x,y,pop,temps,utilisateur);//si on a choisi de demmarer la partie
    if(z=2) then interface02(petitx,petity,gene01,x,y,pop,temps,utilisateur);//si on veut les options
    if(z=3) then//si on veut charger une save
    begin
    tableau := chargeFile(x,y,petitx,petity,temps);//on charge la suvegarde

    (*on initialise la position de la fenetre au milieu*)
    v := ((x div 2 - (petitx div 2))+x)mod x;
    w := ((y div 2 - (petity div 2))+y)mod y;
    deroulement(petitx,petity,x,y,v,w,temps, utilisateur,tableau);//on demmare la partie  
    end;
  end;
end;



(* PRECONDITION,  aucune variable en parametre
                
*) 
(* POSTCONDITION: propose a l'utilisateur de lire le README*)


procedure lireReadme();
var
  touche : char;
begin
  clrscr();
  writeln('');
  writeln('Lecture du Readme recommandé...');
  writeln('appuyez sur une touche pour l ouvrir');
  writeln('appuyez sur echap pour continuer');
  repeat
  until KeyPressed;
  touche := ReadKey;
  if(not(touche = #27)) then
  begin
  writeln('lecture du fichier');
  writeln('');
  fpsystem('less README');
  writeln('');
  end;
end;

(* PRECONDITION,  aucune variable demandé            

POSTCONDITION: crée les fichiers nescessaire au bon fonctionnement du prgm si ils n'existent pas et les initialises*)


procedure Installation();
  var
f:TextFile;
i:integer;
begin
  i:= 0;
  while (not(fileExist('save.config')) and (i<6)) do//tant que le fichier n existe pas mais pas plus de 5 fois
  begin   
      write('creation du fichier...');
      fpsystem('touch save.config');//on va le creer     
      if(fileExist('save.config'))then//si il a été créé
      begin
        assign(f,'save.config');//on ouvre le fichier         
        rewrite(f);//on l'initialise        
        writeln(f,'0');//on ecrit le texte dedans
        writeln('fichier créé');
        close(f);//on le ferme
        lireReadme();//on propose à l'utilidateur de lire le README
      end
      else//si ca n'a pas fonctionné
      begin
        writeln('impossible de le créer');
        i:=i+1;//on tentera une fois de moins
      end;
  end;
  if(i = 6 ) then//si ca a depassé le max, il ya un probleme
  begin
  writeln('erreur, installation impossible, le fichier de config n a pas pu être crée');
  halt;//on quitte tout
  end;  
end;

(*========fonction principale=========*)

begin

  installation();
  randomize;  
  menu02();  
  writeln('');// on saute une ligne pour faire jolie

end.
