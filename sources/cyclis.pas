unit cyclis;

interface

function readMdp(const texte :string):string;
function longNombre(a:integer):integer;
function absolu(a:integer):integer;
function extension(nom:string):string;
function nombreligne(nom:string):integer;
function lireFichier(a:integer;nom:string):string;
function nombreligne02(nom:string):integer;
Function fileExist(nom:string):Boolean;

implementation


uses crt,sysutils;



Function fileExist(nom:string):Boolean; // fonction qui retourne un booleen, true si la fichier existe et false sinon
Var f : text;
Begin
  {$I-} //on ignore les potentielles erreurs
  Assign(f, nom);  //on assigne un ID au fichier
  Reset(f); //on essaye de le lire
  Close(f); // on le ferme
  {$I+} //on réactive le IO/check
  fileExist := (ioresult = 0) ; //on retourne un booleen vrai si le fichier existe
End;




(*PRECONDITION:--nom:chaine de caractere contenant un nom de fichier pouvant être ouvert

 POSTCONDITION:la fonction renvoie le nombre de ligne contenant .pas ,du fichier nommé [nom]

*)

  
function nombreligne(nom:string):integer;
var
  fichier:TextFile;
  taille : integer;
  texte : string;
begin
  Assign(fichier, nom);//on ouvre le fichier
  reset(fichier); //on initaialise le fichier
  texte := 'rqsfqd';//on initialise a chaine
  taille := 0;
  while (not(texte = '')) do//tant qu'on arrive pas a la fin du fichier
  begin
    readln(fichier,texte);//on lit la prochaine ligne
    if(extension(texte) = 'vie') then//si l'extension est valide
    begin
      taille := taille +1;//on augmente le nombre de fichiers repertoriés
    end;
  end;
  Close(fichier);
  nombreligne := taille;
end;



(*
PRECONDITION:--nom:chaine de caractere contenant un nom de fichier pouvant être ouvert

 POSTCONDITION:la fonction renvoie le nombre de ligne du fichier nommé [nom]

*)

function nombreligne02(nom:string):integer;
var
  fichier:TextFile;
  taille : integer;
  texte : string;
begin
  Assign(fichier, nom);//on ouvre le fichier
  reset(fichier); //on initaialise le fichier
  texte := 'rqsfqd';//on initialise a chaine
  taille := 0;
  while (not(eof(fichier))) do//tant qu on arrive pas a la fin du fichier
  begin
    readln(fichier,texte);//on lit la prochaine ligne
    taille := taille +1;//on augmente le nombre de fichiers repertoriés
  end;
  Close(fichier);
  nombreligne02 := taille;
end;






function extension(nom:string):string;
var
mot:string;
lettre :string;
l,i:integer;
ok :boolean;
begin
  ok := false;
  mot := '';
  l := Length(nom);


  for i:=1 to l do
  begin
    lettre := LeftStr(nom,1);   

    if ok then mot := mot + lettre;
    ok := (lettre = '.') or (ok);
    nom:= RightStr(nom,l-i);
  end;
  extension := mot;
end;



function absolu(a:integer):integer;
begin
  if (a>=0) then absolu := a else absolu := -a;
end;


function lireFichier(a:integer;nom:string):string;
var
fichier:TextFile;
texte:string;
i:integer;
begin
Assign(fichier, nom);//on ouvre le fichier
reset(fichier); //on l'initialise
for i:=0 to a-1 do
begin
readln(fichier,texte);
end;
Close(fichier);
lireFichier := texte;
end;

function longNombre(a:integer):integer;
var
i:integer;
begin
  i:=1;
  while a>10 do
  begin
   a:=a div 10;
   i:=i+1;
  end;
  longNombre := i;
end;


function readMdp(const texte :string):string;

var
touche:char;
mdp:string;
x,i:integer;
t0,t:TTimeStamp;
begin
  clrscr();
  write(texte);
  touche := ' ';
  x:=0;
  mdp := '';
  t0:=DateTimeToTimeStamp (Now);
  t:=t0;
  while ((touche <> #13)and((t.time - t0.time)<10000)) do
  begin
    t:=DateTimeToTimeStamp (Now);
    if (KeyPressed) then 
    begin

      touche := ReadKey;
      
      case touche of
        'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8','9':
        begin
          mdp:= mdp + touche;     
          x:=x+1;
          write('*');
        end;
        #0:
        begin
          x:=x-1;
          clrscr();
          write(texte);
          if(x<0) then x:= 0;
          for i:=0 to x-1 do
          begin
          write('*');
          end;
          mdp := LeftStr(mdp,x);
        end;
      end;        

      while keypressed do touche:=Readkey;      
    end;
    
  end;
  writeln();
  writeln(mdp);
  if((t.time - t0.time)>10005) then readMdp := 'stop' else readMdp := mdp;
end;

finalization
  

end.
